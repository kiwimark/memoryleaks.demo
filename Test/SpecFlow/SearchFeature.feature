﻿Feature: SearchFeature
	As a system user 
	I want to be able to search for customers in my database by name
	So that I can view customer information

# Background : A common precondition for all scenarios in a feature file.
Background:

@Search
Scenario: Search for known name
	Given My customer database contains "10" people with the name "David"
	And I have entered "David" into the search screen
	When I search
	Then "10" results are displayed 
	And All results have the name "David"
	
@Search
Scenario: Search for unknown name
	Given My customer database contains "0" people with the name "Sarah"
	And I have entered "Sarah" into the search screen
	When I search
	Then "0" results are displayed 
	
@Search
Scenario: Search for known name with table
	Given My customer database contains 
	| Name  | Age | Gender |
	| David | 40  |	M |
	| David	| 35  |	M |
	And I have entered "David" into the search screen
	When I search
	Then "2" results are displayed 
	And All results have the name "David"

# Notes : Tests can be ignored by adding the @ignore to the Scenario 
