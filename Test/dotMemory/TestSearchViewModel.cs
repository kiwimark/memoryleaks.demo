﻿namespace Test.DotMemory
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Threading;
    using JetBrains.dotMemoryUnit;
    using Moq;
    using NUnit.Framework;
    using Service;
    using Leaky.ViewModel;

    public class TestSearchViewModel
    {
        [DotMemoryUnit(FailIfRunWithoutSupport = false)] // This allows the test to be run without dotMemory support.
        [Test(Description = "Asserts the search view model isn't retained in memory after references have been removed")]
        public void SearchViewModelShouldNotLeakMemory()
        {
            const int NumberOfPersonsToCreate = 10;
            const string PersonsNameToSearchFor = "Andy";

            Mock<IPersonSearchService> mockCustomerSearchService = CreateMockCustomerSearchService(NumberOfPersonsToCreate);

            // Gets a reference to memory data in the current execution point.
            // Use this reference as a base for comparison with memory data taken in other points.
            MemoryCheckPoint memoryCheckPoint1 = dotMemory.Check();

            Debug.WriteLine("Taken the first memory check point");

            // Create the subject under test.
            SearchViewModel searchViewModel = new SearchViewModel(mockCustomerSearchService.Object) { Name = PersonsNameToSearchFor };

            Assert.IsTrue(searchViewModel.SearchCommand.CanExecute(null));

            // Execute a search.
            searchViewModel.SearchCommand.Execute(null);

            // Check we called the service
            Assert.IsNotNull(searchViewModel.Persons);
            Assert.AreEqual(NumberOfPersonsToCreate, searchViewModel.Persons.Count);

            // Make sure the timer has fired and auto refreshed the search results.
            WaitForAutoRefreshWithTimeout(searchViewModel);

            // We're done with the search view model so lets tell it to unload and then lets remove our reference to it.
            searchViewModel.SearchWindowUnloadedCommand.Execute(null);

            // ReSharper disable once RedundantAssignment
            searchViewModel = null;

            // Forces an immediate garbage collection of all generations.
            GC.Collect();

            // Blocks until all objects in the finalisation queue have been finalised 
            // (objects which might have been put there by the previous call to Collect).
            GC.WaitForPendingFinalizers();

            // Check the SearchViewModel isn't retained.
            dotMemory.Check(
                 memory =>
                 {
                     Debug.WriteLine("Taken another memory check point");

                     SnapshotDifference snapshotDifference = memory.GetDifference(memoryCheckPoint1);
                     ObjectSet survivedObjects = snapshotDifference.GetSurvivedObjects();
                     ObjectSet exclusivelyRetainedObjects = survivedObjects.GetExclusivelyRetainedObjects();

                     Debug.WriteLine("Looking for retained SearchViewModel in memory");

                     int numberOfRetainedSearchViewModels = exclusivelyRetainedObjects.GetObjects(
                                                                objectProperty => objectProperty.Type.Is<SearchViewModel>())
                                                                .ObjectsCount;

                     Assert.AreEqual(0, numberOfRetainedSearchViewModels, "Search View Models have been retained");
                 });

            Debug.WriteLine("Finished");
        }

        private void WaitForAutoRefreshWithTimeout(SearchViewModel searchViewModel)
        {
            int autoRefreshInterval = searchViewModel.AutoRefreshInterval;
            int waited = 0;
            int timeOut = autoRefreshInterval * 2;

            while (!searchViewModel.HasAutoRefreshed)
            {
                Thread.Sleep(1000);

                waited += 1000;

                if (waited > timeOut)
                {
                    Assert.Fail("We waited twice as long as the auto refresh interval and the view model didn't auto refresh!");
                }
            }
        }

        private Mock<IPersonSearchService> CreateMockCustomerSearchService(int numberOfPersonsToCreate)
        {
            // Create and setup a mocked customer search service.
            var mockCustomerSearchService = new Mock<IPersonSearchService>();

            mockCustomerSearchService.Setup(service => service.Search(It.IsAny<string>())).Returns(
                delegate (string name)
                {
                    Random random = new Random();
                    List<Person> persons = new List<Person>();

                    for (int i = 0; i < numberOfPersonsToCreate; i++)
                    {
                        char gender = i % 2 == 0 ? 'F' : 'M';
                        int age = random.Next(18, 90);

                        Person person = new Person(name, age, gender);

                        persons.Add(person);
                    }

                    return persons;
                });

            return mockCustomerSearchService;
        }
    }
}