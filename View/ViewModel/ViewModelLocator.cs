namespace Leaky.ViewModel
{
    using Service;

    public class ViewModelLocator
    {
        /// <summary>
        /// Initializes a new instance of the ViewModelLocator class.
        /// </summary>
        public ViewModelLocator()
        {
        }

        /// <summary>
        /// Gets the search view model.
        /// </summary>
        public SearchViewModel SearchViewModel
        {
            get
            {
                return new SearchViewModel(new PersonSearchService());
            }
        }

        /// <summary>
        /// Gets the main view model.
        /// </summary>
        public MainViewModel MainViewModel
        {
            get
            {
                return new MainViewModel();
            }
        }
    }
}