namespace Leaky.ViewModel
{
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;

    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
        {
            SearchCommand = new RelayCommand(SearchExecute, SearchCanExecute);
        }

        /// <summary>
        /// Gets or sets the search command.
        /// </summary>
        public RelayCommand SearchCommand { get; set; }

        private bool SearchCanExecute()
        {
            return true;
        }
        
        private void SearchExecute()
        {
            SearchWindow searchWindow = new SearchWindow();
            searchWindow.ShowDialog();
        }
    }
}