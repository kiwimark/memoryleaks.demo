namespace Leaky.ViewModel
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Timers;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.CommandWpf;
    using Service;
    using Seterlund.CodeGuard;

    public class SearchViewModel : ViewModelBase
    {
        /// <summary>
        /// The person search service
        /// </summary>
        private readonly IPersonSearchService _personSearchService;

        /// <summary>
        /// The name to search for
        /// </summary>
        private string _name;

        /// <summary>
        /// The results collection of persons
        /// </summary>
        private ObservableCollection<Person> _persons;

        /// <summary>
        /// The auto refresh timer
        /// </summary>
        private Timer _timer;

        /// <summary>
        /// The status bar message
        /// </summary>
        private string _statusBarMessage;

        /// <summary>
        /// Initializes a new instance of the <see cref="SearchViewModel"/> class.
        /// </summary>
        /// <param name="personSearchService">The person search service.</param>
        public SearchViewModel(IPersonSearchService personSearchService)
        {
            Guard.That(personSearchService).IsNotNull();

            _personSearchService = personSearchService;
            Persons = new ObservableCollection<Person>();

            HasAutoRefreshed = false;
            AutoRefreshInterval = 5000;

            SearchCommand = new RelayCommand(SearchExecute, SearchCanExecute);
            SearchWindowUnloadedCommand = new RelayCommand(SearchWindowUnloadedExecute);

            SetupAutoRefreshTimer();
        }

        /// <summary>
        /// Gets a value indicating whether the auto refresh has fired.
        /// </summary>
        public bool HasAutoRefreshed { get; private set; }

        /// <summary>
        /// Gets the automatic refresh interval in milliseconds.
        /// </summary>
        public int AutoRefreshInterval { get; }

        /// <summary>
        /// Gets or sets the search command 
        /// </summary>
        public RelayCommand SearchCommand { get; set; }

        /// <summary>
        /// Gets or sets the search window unloaded command.
        /// </summary>
        public RelayCommand SearchWindowUnloadedCommand { get; set; }

        /// <summary>
        /// Gets or sets the persons.
        /// </summary>
        public ObservableCollection<Person> Persons
        {
            get
            {
                return _persons;
            }

            set
            {
                _persons = value;

                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the name to search for.
        /// </summary>
        public string Name
        {
            get
            {
                return _name;
            }

            set
            {
                _name = value;

                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Gets or sets the status bar message.
        /// </summary>
        public string StatusBarMessage
        {
            get
            {
                return _statusBarMessage;
            }

            set
            {
                _statusBarMessage = value;

                RaisePropertyChanged();
            }
        }

        /// <summary>
        /// Conditions that must be true in order to search
        /// </summary>
        /// <returns><c>true</c> A name has been specified.  Otherwise <c>false</c></returns>
        private bool SearchCanExecute()
        {
            return !string.IsNullOrWhiteSpace(Name);
        }

        /// <summary>
        /// Executes a search
        /// </summary>
        private void SearchExecute()
        {
            StatusBarMessage = string.Empty;

            // Call the person search service
            IEnumerable<Person> results = _personSearchService.Search(Name);

            Persons = new ObservableCollection<Person>(results);
        }

        /// <summary>
        /// Setups the automatic refresh timer.
        /// </summary>
        private void SetupAutoRefreshTimer()
        {
            _timer = new Timer();
            _timer.Elapsed += OnTimer;
            _timer.Interval = AutoRefreshInterval;
            _timer.Enabled = true;
            _timer.Start();
        }

        /// <summary>
        /// The auto refresh timer interval elapsed event
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnTimer(object sender, EventArgs e)
        {
            // Auto refresh the grid.
            if (SearchCommand.CanExecute(null))
            {
                SearchCommand.Execute(null);

                HasAutoRefreshed = true;

                StatusBarMessage = "Auto refreshed";
            }
        }

        private void SearchWindowUnloadedExecute()
        {
            if (_timer != null)
            {
                _timer.Elapsed -= OnTimer;
            }
        }
    }
}