﻿namespace Service
{
    using System.Collections.Generic;

    /// <summary>
    /// Person search service
    /// </summary>
    public interface IPersonSearchService
    {
        /// <summary>
        /// Searches the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <returns>A list of persons</returns>
        IEnumerable<Person> Search(string name);
    }
}